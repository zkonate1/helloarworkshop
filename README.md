# HelloAR Workshop May 2019 Participant Information:

#### Your computer  should be equipped with the following:

  

From the Apple Store: 

MAC OS Mojave 10.14    

Xcode 10.2.   

  
  

Latest  Version of Unity   version 2019.1.4 (may 25)   Personal or Pro edition  

[GO to archive page ](https://unity3d.com/get-unity/download/archive)and select the latest version (2019.1.4)

  

Click on “Downloads (Mac)” or Downloads (Windows) to download installer.  

IMPORTANT:  make sure to include IOS support component when you set up the download. 

Download The Workshop Example Project (which includes ARKit)

[Download Unity Project here](https://gitlab.com/zkonate1/helloarworkshop/-/archive/master/helloarworkshop-master.zip)

  
  

#### Your AR device should be compatible. Check below to make sure:

  

AR [compatible device](https://developer.apple.com/library/archive/documentation/DeviceInformation/Reference/iOSDeviceCompatibility/DeviceCompatibilityMatrix/DeviceCompatibilityMatrix.html)s are :

• iPhone 6s and 6s Plus

• iPhone 7 and 7 Plus

• iPhone SE

• iPad Pro (9.7, 10.5 or 12.9) – both first-gen and 2nd-gen

• iPad (2017)

• iPhone 8 and 8 Plus

• iPhone X

  
  

Update your device to  IOS 12  if necessary

  

DO NOT FORGET TO BRING THE DEVICE’S USB CABLE